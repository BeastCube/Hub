package net.beastcube.hub.network;

import net.beastcube.api.network.MessageHandler;
import net.beastcube.api.network.message.SlaveConnectedMessage;
import net.beastcube.hub.Hub;

/**
 * @author BeastCube
 */
public class IncomingMessageHandler {

    @MessageHandler
    public void onSlaveConnectedMessage(SlaveConnectedMessage message) {
        Hub.registerLobby();
    }

}