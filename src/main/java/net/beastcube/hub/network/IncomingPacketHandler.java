package net.beastcube.hub.network;

import com.ikeirnez.pluginmessageframework.packet.PacketHandler;
import net.beastcube.api.bukkit.menus.items.*;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.api.bukkit.menus.menus.PagedMenu;
import net.beastcube.api.bukkit.util.ItemBuilder;
import net.beastcube.api.commons.packets.UpdateScoreboardPacket;
import net.beastcube.api.commons.packets.friends.ShowFriendsPacket;
import net.beastcube.api.commons.packets.friends.ShowPlayerPacket;
import net.beastcube.hub.Hub;
import net.beastcube.hub.hubgadgets.friends.PlayerMenuItem;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Collections;

/**
 * @author BeastCube
 */
public class IncomingPacketHandler {

    private Hub plugin;

    public IncomingPacketHandler(Hub plugin) {
        this.plugin = plugin;
    }

    @SuppressWarnings("deprecation")
    @PacketHandler
    public void onShowPlayerPacket(Player player, ShowPlayerPacket packet) {
        if (player.isOnline()) {
            ItemMenu menu = new ItemMenu(ChatColor.AQUA + packet.getName(), ItemMenu.Size.ONE_LINE, plugin);

            menu.setItem(4, new MenuItem(ChatColor.YELLOW + packet.getName(), new ItemBuilder(Material.SKULL_ITEM).durability(SkullType.PLAYER.ordinal()).skullOwner(packet.getName()).build()));
            //----------- Friends ----------------
            if (packet.isFriend()) {
                menu.setItem(0, new BungeeCommandItem("Freund entfernen", new ItemStack(Material.BARRIER), "friends remove " + packet.getName()));
            } else {
                if (packet.isRequested()) {
                    menu.setItem(0, new BungeeCommandItem("Freundschaftsanfrage annehmen", new ItemStack(Material.WOOL, 1, DyeColor.GREEN.getData()), "friends accept " + packet.getName()));
                    menu.setItem(1, new BungeeCommandItem("Freundschaftsanfrage ablehnen", new ItemStack(Material.WOOL, 1, DyeColor.RED.getData()), "friends decline " + packet.getName()));
                } else if (packet.hasRequested()) {
                    menu.setItem(0, new BungeeCommandItem("Freundschaftsanfrage abbrechen", new ItemStack(Material.WOOL, 1, DyeColor.RED.getData()), "friends abort " + packet.getName()));
                } else {
                    menu.setItem(0, new BungeeCommandItem("Freundschaftsanfrage schicken", new ItemStack(Material.SKULL_ITEM), "friends invite " + packet.getName()));
                }
            }
            if (packet.isOnline() && packet.isFriend()) {
                menu.setItem(7, new BungeeCommandItem("Zum Spieler teleportieren", new ItemStack(Material.ENDER_PEARL), "friends teleport " + packet.getName()));
            }
            //----------- Party ----------------
            if (packet.isOnline())
                //TODO If inParty leave party or kick..
                menu.setItem(8, new BungeeCommandItem("In Party einladen", new ItemStack(Material.FIREWORK), "party invite " + packet.getName()));

            menu.open(player);
        }
    }

    @PacketHandler
    public void onShowFriendsPacket(Player player, ShowFriendsPacket packet) {
        if (player.isOnline()) {
            PagedMenu menu = new PagedMenu(ChatColor.GOLD + "Freunde", ItemMenu.Size.SIX_LINE, plugin);

            Collections.sort(packet.getFriends(), (friend1, friend2) -> Boolean.compare(!friend1.isOnline(), !friend2.isOnline()));

            for (int i = 0; i < packet.getFriends().size(); i++) {
                menu.setItem(i, new PlayerMenuItem(packet.getFriends().get(i)));
            }
            menu.setMenuItem(0, new PreviousPageItem());
            menu.setMenuItem(8, new NextPageItem());
            menu.setMenuItem(4, new CurrentPageItem());
            menu.open(player);
        }
    }

    @PacketHandler
    public void onUpdateScoreboardPacket(Player player, UpdateScoreboardPacket packet) {
        Hub.updateScoreBoard(player, packet.getFriendCount());
    }

}