package net.beastcube.hub.hubgadgets.friends;

import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.bukkit.interactitem.InteractItem;
import net.beastcube.api.commons.packets.friends.ShowFriendsRequestPacket;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Collections;
import java.util.List;

/**
 * @author BeastCube
 */
public class FriendsMenu extends InteractItem {

    @Override
    public void execute(Player p, JavaPlugin plugin, Object data) {
        BeastCubeAPI.getGateway().sendPacket(p, new ShowFriendsRequestPacket());
    }

    @Override
    public List<String> getLore() {
        return Collections.singletonList(ChatColor.GRAY + "Sehe und verwalte deine Freunde");
    }

    @Override
    public String getDisplayName() {
        return ChatColor.YELLOW + "Freunde";
    }

    @Override
    public Material getMaterial() {
        return Material.SKULL_ITEM;
    }

    @Override
    public byte getData() {
        return (byte) SkullType.PLAYER.ordinal();
    }
}
