package net.beastcube.hub.hubgadgets.friends;

import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.bukkit.menus.events.ItemClickEvent;
import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.api.bukkit.util.ItemBuilder;
import net.beastcube.api.commons.packets.friends.Friend;
import net.beastcube.api.commons.packets.friends.ShowPlayerRequestPacket;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public class PlayerMenuItem extends MenuItem {
    private final Friend friend;
    public static String TITLE = ChatColor.YELLOW + "Friends";

    public PlayerMenuItem(Friend friend) {
        super(TITLE, new ItemBuilder(Material.SKULL_ITEM).name(TITLE).build());
        this.friend = friend;
    }

    @Override
    public ItemStack getFinalIcon(Player player, ItemMenu menu) {
        ItemBuilder ib = new ItemBuilder(Material.SKULL_ITEM);
        if (friend.isOnline()) {
            ib.skullOwner(friend.getName()).durability(SkullType.PLAYER.ordinal())
                    .name(ChatColor.GREEN + friend.getName())
                    .lore(ChatColor.GREEN + "Online auf " + ChatColor.GOLD + friend.getCurrentServer());
        } else {
            ib.name(ChatColor.DARK_GRAY + friend.getName())
                    .lore(ChatColor.RED + "Offline");
        }
        return ib.build();
    }

    @Override
    public void onItemClick(ItemClickEvent e) {
        BeastCubeAPI.getGateway().sendPacket(e.getPlayer(), new ShowPlayerRequestPacket(friend.getUniqueId()));
        e.setAction(ItemClickEvent.ItemClickAction.CLOSE);
    }

}
