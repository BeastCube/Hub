package net.beastcube.hub.hubgadgets.teleporter;

import net.beastcube.api.bukkit.interactitem.InteractItem;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.hub.Hub;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Collections;
import java.util.List;

public class Teleporter extends InteractItem {
    public static String TITLE = ChatColor.BLUE + "Teleporter";

    @Override
    public void execute(Player p, JavaPlugin plugin, Object data) {
        List<TeleportLocation> warps = Hub.getInstance().getHubSettings().getWarps();
        ItemMenu menu = new ItemMenu(TITLE, ItemMenu.Size.ONE_LINE, plugin);
        for (int i = 0; i < warps.size(); i++) {
            menu.setItem(i, new TeleporterMenuItem(warps.get(i)));
        }
        menu.open(p);
    }

    @Override
    public List<String> getLore() {
        return Collections.singletonList(ChatColor.GRAY + "Teleportiert dich zum ausgewählten Spiel");
    }

    @Override
    public String getDisplayName() {
        return ChatColor.YELLOW + "Teleporter";
    }

    @Override
    public Material getMaterial() {
        return Material.COMPASS;
    }

}
