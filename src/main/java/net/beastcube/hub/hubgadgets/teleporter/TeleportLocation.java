package net.beastcube.hub.hubgadgets.teleporter;

import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

public class TeleportLocation {

    @Getter
    ItemStack itemStack;
    @Getter
    Location location;

    public TeleportLocation(ItemStack stack, Location loc) {
        itemStack = stack;
        location = loc;
    }

    public String getDisplayName() {
        if (itemStack.hasItemMeta())
            if (itemStack.getItemMeta().hasDisplayName())
                return itemStack.getItemMeta().getDisplayName();
        return "";
    }

}
