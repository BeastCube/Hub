package net.beastcube.hub.hubgadgets.teleporter;

import net.beastcube.api.bukkit.menus.events.ItemClickEvent;
import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public class TeleporterMenuItem extends MenuItem {

    private final TeleportLocation loc;

    public TeleporterMenuItem(TeleportLocation loc) {
        super(loc.getDisplayName(), loc.getItemStack());
        this.loc = loc;

    }

    @Override
    public void onItemClick(ItemClickEvent e) {
        e.getPlayer().teleport(loc.getLocation());
        e.setAction(ItemClickEvent.ItemClickAction.CLOSE);
    }

    @Override
    public ItemStack getFinalIcon(Player player, ItemMenu menu) {
        return loc.getItemStack();
    }
}

