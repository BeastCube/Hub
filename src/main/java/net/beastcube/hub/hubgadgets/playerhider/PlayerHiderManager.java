package net.beastcube.hub.hubgadgets.playerhider;

import net.beastcube.api.commons.database.FriendsDatabaseHandler;
import net.beastcube.api.commons.permissions.Roles;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * @author BeastCube
 */
public class PlayerHiderManager {

    public PlayerHider playerHider;
    public static HashMap<UUID, PlayerVisibilityType> hiddenPlayers = new HashMap<>();

    public PlayerHiderManager() {
        playerHider = new PlayerHider();
    }

    public static void updateVisibility(Player p) { //TODO Update on Friend Accepted and Rank changed (PluginMessage)
        List<UUID> uuidlist = FriendsDatabaseHandler.getFriendList(p.getUniqueId());
        List<UUID> staffuuidlist = Roles.getPlayersWithRole();
        Bukkit.getOnlinePlayers().stream().filter(player -> PlayerHiderManager.hiddenPlayers.containsKey(player.getUniqueId())).filter(player -> PlayerHiderManager.hiddenPlayers.get(player.getUniqueId()) != PlayerVisibilityType.NONE).forEach(player -> {
            PlayerVisibilityType type = PlayerHiderManager.hiddenPlayers.get(player.getUniqueId());
            if (type == PlayerVisibilityType.ALL) {
                player.hidePlayer(p);
            } else if (type == PlayerVisibilityType.FRIENDS) {
                if (!uuidlist.contains(player.getUniqueId())) {
                    player.hidePlayer(p);
                }
            } else if (type == PlayerVisibilityType.STAFF) {
                if (!staffuuidlist.contains(player.getUniqueId())) {
                    player.hidePlayer(p);
                }
            }
        });
    }

}
