package net.beastcube.hub.hubgadgets.playerhider;

import net.beastcube.api.bukkit.menus.events.ItemClickEvent;
import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.api.commons.database.FriendsDatabaseHandler;
import net.beastcube.api.commons.permissions.Roles;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author BeastCube
 */
public class PlayerHiderMenuItem extends MenuItem {
    private static final String DISPLAY_NAME = ChatColor.BLUE + "Player Hider";
    private static final ItemStack ICON = new ItemStack(Material.STICK);
    private final PlayerVisibilityType type;

    public PlayerHiderMenuItem(PlayerVisibilityType type) {
        super(DISPLAY_NAME, ICON);
        this.type = type;
    }

    // This method controls what happens when the MenuItem is clicked
    @Override
    public void onItemClick(ItemClickEvent event) {
        Collection<Player> list = new ArrayList<>();
        switch (type) {
            case ALL:
                list.addAll(Bukkit.getOnlinePlayers().stream().collect(Collectors.toList()));
                break;
            case FRIENDS:
                List<UUID> uuidlist = FriendsDatabaseHandler.getFriendList(event.getPlayer().getUniqueId());
                list.addAll(Bukkit.getOnlinePlayers().stream().filter(player -> !uuidlist.contains(player.getUniqueId())).collect(Collectors.toList()));
                break;
            case STAFF:
                List<UUID> staffuuidlist = Roles.getPlayersWithRole();
                list.addAll(Bukkit.getOnlinePlayers().stream().filter(player -> !staffuuidlist.contains(player.getUniqueId())).collect(Collectors.toList()));
                break;
        }
        if (type == PlayerVisibilityType.NONE) {
            if (PlayerHiderManager.hiddenPlayers.containsKey(event.getPlayer().getUniqueId()))
                PlayerHiderManager.hiddenPlayers.remove(event.getPlayer().getUniqueId());
            for (Player player : Bukkit.getOnlinePlayers()) {
                event.getPlayer().showPlayer(player);
            }
        } else {
            PlayerHiderManager.hiddenPlayers.put(event.getPlayer().getUniqueId(), type);
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (list.contains(player)) {
                    event.getPlayer().hidePlayer(player);
                } else {
                    event.getPlayer().showPlayer(player);
                }
            }
        }
        event.setAction(ItemClickEvent.ItemClickAction.CLOSE);
    }

    // This method lets you modify the ItemStack before it is displayed, based on the player opening the menu
    @Override
    public ItemStack getFinalIcon(Player player, ItemMenu menu) {
        String displayName = "";
        Material material;
        switch (type) {
            case ALL:
                displayName = ChatColor.GREEN + "Verstecke alle Spieler";
                break;
            case NONE:
                displayName = ChatColor.GREEN + "Zeige alle Spieler";
                break;
            case FRIENDS:
                displayName = ChatColor.GREEN + "Zeige nur Freunde";
                break;
            case STAFF:
                displayName = ChatColor.GREEN + "Zeige nur Youtuber, Premium- und Teammitglieder";
                break;
        }
        if (PlayerHiderManager.hiddenPlayers.containsKey(player.getUniqueId())) {
            if (type == PlayerHiderManager.hiddenPlayers.get(player.getUniqueId())) {
                material = Material.SUGAR;
            } else {
                material = Material.SULPHUR;
            }
        } else {
            if (type == PlayerVisibilityType.NONE) {
                material = Material.SUGAR;
            } else {
                material = Material.SULPHUR;
            }
        }
        ItemStack itemstack = new ItemStack(material);
        ItemMeta meta = itemstack.getItemMeta();
        meta.setDisplayName(displayName);
        itemstack.setItemMeta(meta);
        return itemstack;
    }

}
