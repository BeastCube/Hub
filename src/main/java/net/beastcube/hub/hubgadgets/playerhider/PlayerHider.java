package net.beastcube.hub.hubgadgets.playerhider;

import net.beastcube.api.bukkit.interactitem.InteractItem;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * @author BeastCube
 */
public class PlayerHider extends InteractItem {

    @Override
    public void execute(Player p, JavaPlugin plugin, Object data) {
        ItemMenu menu = new ItemMenu(ChatColor.YELLOW + "Spieler verstecken", ItemMenu.Size.ONE_LINE, plugin);
        menu.setItem(1, new PlayerHiderMenuItem(PlayerVisibilityType.NONE));
        menu.setItem(3, new PlayerHiderMenuItem(PlayerVisibilityType.ALL));
        menu.setItem(5, new PlayerHiderMenuItem(PlayerVisibilityType.FRIENDS));
        menu.setItem(7, new PlayerHiderMenuItem(PlayerVisibilityType.STAFF));
        menu.open(p);
    }

    @Override
    public List<String> getLore() {
        return Collections.singletonList(ChatColor.GRAY + "Schaltet die Sichtbarkeit der Spieler um");
    }

    @Override
    public String getDisplayName() {
        return ChatColor.YELLOW + "Spieler verstecken";
    }

    @Override
    public Material getMaterial() {
        return Material.STICK;
    }

    @Override
    public HashMap<Enchantment, Integer> getEnchantments() {
        return new HashMap<Enchantment, Integer>() {{
            put(Enchantment.KNOCKBACK, 10);
        }};
    }

}
