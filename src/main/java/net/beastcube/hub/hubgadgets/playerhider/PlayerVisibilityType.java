package net.beastcube.hub.hubgadgets.playerhider;

/**
 * @author BeastCube
 */
public enum PlayerVisibilityType {
    NONE,
    ALL,
    STAFF,
    FRIENDS
}
