package net.beastcube.hub.hubgadgets.stacker;

import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

/**
 * @author BeastCube
 */
public class StackerManager {

    public static void stack(Player stacker, Player stacked) {
        /*if (!pl.stackerEnabled.contains(p.getName()))
        {
            p.sendMessage(ChatColor.GREEN + "Stacker" + ChatColor.BOLD + "\u15CC" + ChatColor.RESET + ChatColor.BLUE + " You do not have stacking turned on!");
            return;
        }*/

        Entity entityStackTip = stacker, toStack = stacked;

        while (toStack.getVehicle() != null) {
            toStack = toStack.getVehicle();
        }

        while (entityStackTip.getPassenger() != null) {
            entityStackTip = entityStackTip.getPassenger();
        }

        entityStackTip.setPassenger(toStack);

        stacker.sendMessage(ChatColor.GREEN + "Stacker" + ChatColor.BOLD + "\u15CC" + ChatColor.RESET + ChatColor.GOLD + " Du hast " + stacked.getName() + " gestackt.");
        if (toStack instanceof Player)
            toStack.sendMessage(ChatColor.GREEN + "Stacker" + ChatColor.BOLD + "\u15CC" + ChatColor.RESET + ChatColor.GOLD + " Du wurdest von " + stacker.getName() + " gestackt, sneake um herunterzukommen.");
    }

    public static boolean throwStack(Player p) {
        boolean stacking = false;
        if (p.getVehicle() == null) {
            Entity stacked = p.getPassenger();
            while (stacked != null) {
                stacking = true;
                stacked.leaveVehicle();
                multiplyVelocity(stacked, 1.8D, 2.0D, 0.3D, p.getLocation().getDirection(), 3D);
                stacked = stacked.getPassenger();
            }
        }
        return stacking;
    }

    private static void multiplyVelocity(Entity e, double strength, double yVel, double yVelMaximum, Vector prevVector, double yMultiply) {
        if (prevVector.length() != 0.0D) {
            prevVector.normalize();
            prevVector.multiply(strength);

            prevVector.setY(prevVector.getY() + yVel);

            if (prevVector.getY() > yVelMaximum) prevVector.setY(yVelMaximum);

            if (e.isOnGround()) prevVector.setY(prevVector.getY() + yMultiply);

            e.setVelocity(prevVector);
        }
    }

}
