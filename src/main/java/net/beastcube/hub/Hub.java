package net.beastcube.hub;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import lombok.Getter;
import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.bukkit.BeastCubeSlave;
import net.beastcube.api.bukkit.interactitem.InteractItem;
import net.beastcube.api.bukkit.interactitem.InteractItemManager;
import net.beastcube.api.bukkit.util.scoreboard.SimpleScoreboard;
import net.beastcube.api.bukkit.util.serialization.Serializer;
import net.beastcube.api.commons.minigames.MinigameType;
import net.beastcube.api.commons.players.Coins;
import net.beastcube.api.commons.stats.Stat;
import net.beastcube.api.network.message.AddLobbyMessage;
import net.beastcube.hub.hubgadgets.friends.FriendsMenu;
import net.beastcube.hub.hubgadgets.playerhider.PlayerHiderManager;
import net.beastcube.hub.hubgadgets.teleporter.TeleportLocation;
import net.beastcube.hub.hubgadgets.teleporter.Teleporter;
import net.beastcube.hub.listeners.*;
import net.beastcube.hub.network.IncomingMessageHandler;
import net.beastcube.hub.network.IncomingPacketHandler;
import net.beastcube.hub.settings.HubConfig;
import net.beastcube.hub.settings.LobbySettings;
import net.beastcube.hub.settings.TeleportLocationTypeAdapter;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * @author BeastCube
 */
public class Hub extends JavaPlugin implements Listener {

    @Getter
    private static Hub instance;

    public static final InteractItem LOBBYTELEPORTER = new Teleporter();
    public static final InteractItem FRIENDSMENU = new FriendsMenu();
    public static PlayerHiderManager playerHiderManager;
    @Getter
    private HubConfig hubConfig;
    @Getter
    private LobbySettings hubSettings = new LobbySettings();
    private Map<UUID, List<Hologram>> statsHolograms = new HashMap<>();

    public void saveSettings() {
        if (getInstance().getHubConfig().currentHub != null) {
            try {
                File file = new File(getInstance().getHubConfig().currentHub.getWorldFolder() + "/lobby.json");
                FileUtils.writeStringToFile(file, Serializer.formatPretty(Serializer.serialize(hubSettings)), "UTF-8");
            } catch (IOException e) {
                getLogger().warning("Failed to save lobby.json!");
                e.printStackTrace();
            }
        } else {
            getLogger().warning("Failed to save lobby settings: currentHub not set");
        }
    }

    public void loadSettings() {
        if (getInstance().getHubConfig().currentHub != null) {
            File file = new File(getInstance().getHubConfig().currentHub.getWorldFolder() + "/lobby.json");
            try {
                if (file.exists()) {
                    String s = FileUtils.readFileToString(file, "UTF-8");
                    hubSettings = Serializer.deserialize(s, LobbySettings.class);
                } else {
                    LobbySettings.class.newInstance();
                }
            } catch (IOException | InstantiationException | IllegalAccessException e) {
                getLogger().warning("Failed to load lobby.json!");
                e.printStackTrace();
            }
        } else {
            getLogger().warning("Failed to save Lobby settings: currentHub not set");
        }
    }

    @Override
    public void onEnable() {
        instance = this;
        hubConfig = new HubConfig(this);
        BeastCubeAPI.getGateway().registerListener(new IncomingPacketHandler(this));

        registerEvents();

        playerHiderManager = new PlayerHiderManager();

        InteractItemManager.registerInteractItem(LOBBYTELEPORTER);
        InteractItemManager.registerInteractItem(playerHiderManager.playerHider);
        InteractItemManager.registerInteractItem(FRIENDSMENU);
        BeastCubeAPI.getMinigamesManager().init();
        BeastCubeAPI.getCommandHandler().registerCommands(new LobbyCommands(this));

        Serializer.registerTypeAdapter(TeleportLocation.class, new TeleportLocationTypeAdapter());
        loadLobby();

        BeastCubeSlave.getInstance().getServer().getMessenger().registerListener(new IncomingMessageHandler());
        registerLobby();
    }

    public static void registerLobby() {
        BeastCubeSlave.getInstance().getServer().getMasterServerInfo().sendMessage(new AddLobbyMessage());
    }

    @Override
    public void onDisable() {

    }

    private void registerEvents() {
        Bukkit.getPluginManager().registerEvents(this, this);
        Bukkit.getPluginManager().registerEvents(new onPlayerJoinEvent(this), this);
        Bukkit.getPluginManager().registerEvents(new onEntityDamageEvent(this), this);
        Bukkit.getPluginManager().registerEvents(new onEntityDamageByEntityEvent(this), this);
        Bukkit.getPluginManager().registerEvents(new onPlayerPickupItemEvent(this), this);
        Bukkit.getPluginManager().registerEvents(new onBlockBreakEvent(this), this);
        Bukkit.getPluginManager().registerEvents(new onPlayerMoveEvent(this), this);
        Bukkit.getPluginManager().registerEvents(new onPlayerInteractEvent(this), this);
        Bukkit.getPluginManager().registerEvents(new onPlayerInteractEntityEvent(this), this);
        Bukkit.getPluginManager().registerEvents(new onEntityExplodeEvent(this), this);
        Bukkit.getPluginManager().registerEvents(new onBlockPlaceEvent(this), this);
        Bukkit.getPluginManager().registerEvents(new onFoodLevelChangeEvent(this), this);
        Bukkit.getPluginManager().registerEvents(new onPlayerDropItemEvent(this), this);
        Bukkit.getPluginManager().registerEvents(new onWeatherChangeEvent(this), this);
        Bukkit.getPluginManager().registerEvents(new onPlayerQuitEvent(this), this);
        Bukkit.getPluginManager().registerEvents(new onPlayerInteractFakeMobEvent(this), this);
        Bukkit.getPluginManager().registerEvents(new onPlayerBucketEmptyEvent(this), this);
    }

    public void loadLobby() {
        loadSettings();

        Bukkit.getOnlinePlayers().forEach(this::displayStats);
    }

    public void displayStats(Player p) {
        removeStats(p);
        statsHolograms.put(p.getUniqueId(), new ArrayList<>());
        Map<MinigameType, Optional<Map<Stat, Long>>> cache = new HashMap<>(); //not used yet, only if we add multiple holograms for a single minigame it's useful
        hubSettings.getStats().forEach((minigameType, loc) -> {
            Hologram statsHologram = HologramsAPI.createHologram(this, loc);
            statsHolograms.get(p.getUniqueId()).add(statsHologram);
            statsHologram.getVisibilityManager().showTo(p);
            statsHologram.getVisibilityManager().setVisibleByDefault(false);
            statsHologram.appendTextLine(ChatColor.GOLD + minigameType.getDisplayName());

            Optional<Map<Stat, Long>> stats;
            if (!cache.containsKey(minigameType)) {
                stats = minigameType.getStats().getStats(p.getUniqueId());
                cache.put(minigameType, stats);
            } else {
                stats = cache.get(minigameType);
            }

            if (stats.isPresent()) {
                stats.get().keySet().forEach(stat -> statsHologram.appendTextLine(stat.getDisplayName() + ": " + ChatColor.GOLD + stat.format(stats.get().get(stat))));
            } else {
                statsHologram.appendTextLine("Du hast noch keine Runde " + minigameType.getDisplayName() + " gespielt.");
                statsHologram.appendTextLine("Spiele eine Runde um deine Statistik freizuschalten.");
            }
            statsHologram.teleport(statsHologram.getLocation().add(new Vector(0, statsHologram.getHeight(), 0)));
        });
    }

    public void removeStats(Player p) {
        if (statsHolograms.containsKey(p.getUniqueId())) {
            statsHolograms.get(p.getUniqueId()).forEach(Hologram::delete);
            statsHolograms.remove(p.getUniqueId());
        }
    }

    @EventHandler
    public void onHangingBreak(HangingBreakEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerPunchFrame(EntityDamageByEntityEvent e) {
        if (e.getEntityType() == EntityType.ITEM_FRAME) {
            if (e.getDamager() instanceof Player) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlayerClickFrame(PlayerInteractEntityEvent e) {
        if (!e.getPlayer().isOp()) {
            if (e.getRightClicked().getType() == EntityType.ITEM_FRAME) {
                e.setCancelled(true);
            }
        }
    }

    public static void updateScoreBoard(Player p, int friendCount) {
        SimpleScoreboard sb = new SimpleScoreboard(ChatColor.GREEN + "BeastCube");
        //TODO A name for the coins (Cubics?...)
        sb.setLine(ChatColor.RED + "Coins:", Coins.getCoins(p.getUniqueId()));
        sb.setLine(ChatColor.RED + "Rang:", 1);
        sb.setLine(ChatColor.RED + "Freunde Online:", friendCount);
        sb.build();
        sb.send(p);
    }

}
