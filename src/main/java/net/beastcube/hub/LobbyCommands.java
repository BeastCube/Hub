package net.beastcube.hub;

import com.google.common.base.Joiner;
import net.beastcube.api.bukkit.commands.Arg;
import net.beastcube.api.bukkit.commands.Command;
import net.beastcube.api.commons.minigames.MinigameType;
import net.beastcube.api.commons.permissions.Permissions;
import net.beastcube.hub.hubgadgets.teleporter.TeleportLocation;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Map;

/**
 * @author BeastCube
 */
public class LobbyCommands {

    Hub plugin;

    public LobbyCommands(Hub plugin) {
        this.plugin = plugin;
    }

    @Command(identifiers = "bchub sethub", permissions = {Permissions.HUB_ADMIN}, description = "Set the current world as hub")
    private void setHub(Player p) {
        Hub.getInstance().getHubConfig().currentHub = p.getLocation().getWorld();
        Hub.getInstance().getHubConfig().save();
        Hub.getInstance().loadLobby();
        p.sendMessage(ChatColor.GREEN + "Hub erfolgreich gesetzt und geladen");
    }

    @Command(identifiers = "bchub setspawn", permissions = {Permissions.HUB_ADMIN}, description = "Set the current position as spawn")
    private void setSpawn(Player p) {
        if (Hub.getInstance().getHubConfig().currentHub != p.getLocation().getWorld()) {
            setHub(p);
        }
        Hub.getInstance().getHubSettings().setSpawn(p.getLocation());
        Hub.getInstance().saveSettings();
        p.sendMessage(ChatColor.GREEN + "Spawn erfolgreich gesetzt");
    }

    @Command(identifiers = "bchub warps list", permissions = {Permissions.HUB_ADMIN}, description = "Show a list of all lobby warps")
    private void listLobbyWarp(Player p) {
        p.sendMessage(ChatColor.GREEN + "----------Warps-----------");
        List<TeleportLocation> warps = Hub.getInstance().getHubSettings().getWarps();
        for (int i = 0; i < warps.size(); i++) {
            p.sendMessage(ChatColor.GREEN + "[" + i + "]" + ChatColor.RESET + " " + warps.get(i).getDisplayName());
        }
    }

    @Command(identifiers = "bchub warps add", permissions = {Permissions.HUB_ADMIN}, description = "Add a lobby warp")
    private void addLobbyWarp(Player p) {
        if (p.getItemInHand() != null && p.getItemInHand().getType() != Material.AIR) {
            ItemStack stack = p.getItemInHand();
            Hub.getInstance().getHubSettings().addWarp(new TeleportLocation(stack, p.getLocation()));
            Hub.getInstance().saveSettings();
            p.sendMessage(ChatColor.GREEN + "Warp erfolgreich hinzugefügt");
        } else {
            p.sendMessage(ChatColor.RED + "Du hast kein Item in der Hand");
        }
    }

    @Command(identifiers = "bchub warps remove", permissions = {Permissions.HUB_ADMIN}, description = "Remove a lobby warp")
    private void removeLobbyWarp(Player p, @Arg(name = "index") int index) {
        List<TeleportLocation> warps = Hub.getInstance().getHubSettings().getWarps();
        if (warps.size() > index) {
            Hub.getInstance().getHubSettings().removeWarp(index);
            Hub.getInstance().saveSettings();
            p.sendMessage(ChatColor.GREEN + "Warp erfolgreich entfernt");
        } else {
            p.sendMessage(ChatColor.RED + "Der angegebene Warp existiert nicht");
        }
    }

    @Command(identifiers = "bchub stats list", permissions = {Permissions.HUB_ADMIN}, description = "Show a list of all lobby stats")
    private void listStats(Player p) {
        p.sendMessage(ChatColor.GREEN + "----------Stats-----------");
        Map<MinigameType, Location> stats = Hub.getInstance().getHubSettings().getStats();
        int i = 0;
        for (Map.Entry<MinigameType, Location> entry : stats.entrySet()) {
            p.sendMessage(ChatColor.GREEN + "[" + i + "]" + ChatColor.RESET + " " + entry.getKey());
            i++;
        }
    }

    @Command(identifiers = "bchub stats add", permissions = {Permissions.HUB_ADMIN}, description = "Add a lobby stat")
    private void addStat(Player p, @Arg(name = "minigame") String minigame) {
        try {
            MinigameType minigameType = MinigameType.valueOf(minigame.toUpperCase());
            Hub.getInstance().getHubSettings().addStatsHologram(minigameType, p.getLocation());
            Hub.getInstance().saveSettings();
            Bukkit.getOnlinePlayers().forEach(player -> Hub.getInstance().displayStats(player));
            p.sendMessage("Stats Hologramm erfolgreich gesetzt");
        } catch (IllegalArgumentException e) {
            p.sendMessage("Dieses Minigame existiert nicht! Verfügbar:");
            p.sendMessage(Joiner.on(", ").join(MinigameType.values()));
        }
    }

    @Command(identifiers = "bchub stats remove", permissions = {Permissions.HUB_ADMIN}, description = "Remove a lobby stat")
    private void removeStat(Player p, @Arg(name = "index") int index) {
        Map<MinigameType, Location> stats = Hub.getInstance().getHubSettings().getStats();
        if (stats.size() > index) {
            Hub.getInstance().getHubSettings().removeStatsHologram(index);
            Hub.getInstance().saveSettings();
            Bukkit.getOnlinePlayers().forEach(player -> Hub.getInstance().displayStats(player));
            p.sendMessage(ChatColor.GREEN + "Stats Hologramm erfolgreich entfernt");
        } else {
            p.sendMessage(ChatColor.RED + "Das angegebene Stats Hologramm existiert nicht");
        }
    }

}
