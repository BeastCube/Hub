package net.beastcube.hub.listeners;

import net.beastcube.hub.hubgadgets.stacker.StackerManager;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.Plugin;

/**
 * @author BeastCube
 */
public class onPlayerInteractEvent implements Listener {

    private final Plugin plugin;

    public onPlayerInteractEvent(Plugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (e.getAction() == Action.LEFT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_AIR) {
            Material material = e.getPlayer().getItemInHand().getType();
            if (material == Material.AIR) {
                if (StackerManager.throwStack(e.getPlayer()))
                    e.setCancelled(true);
            }
        }
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Material material = e.getClickedBlock().getType();
            if (material == Material.TRAP_DOOR) {
                e.setCancelled(true);
            }
        }
    }
}
