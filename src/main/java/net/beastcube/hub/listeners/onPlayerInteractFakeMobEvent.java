package net.beastcube.hub.listeners;

import com.google.common.base.Joiner;
import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.bukkit.fakemob.event.PlayerInteractFakeMobEvent;
import net.beastcube.api.bukkit.minigames.MinigamesManager;
import net.beastcube.api.commons.minigames.MinigameType;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

/**
 * @author BeastCube
 */
public class onPlayerInteractFakeMobEvent implements Listener {

    private final Plugin plugin;

    public onPlayerInteractFakeMobEvent(Plugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerInteractFakeMob(PlayerInteractFakeMobEvent e) {
        final Player p = e.getPlayer();
        final String interactAction = e.getMob().getInteractAction();
        final String mobData = e.getMob().getMobData();
        if (!e.isCancelled() && e.getAction() == PlayerInteractFakeMobEvent.Action.RIGHT_CLICK) {
            if (interactAction.equalsIgnoreCase("minigameMenu")) {
                try {
                    MinigamesManager.minigameInventories.get(MinigameType.valueOf(mobData.toUpperCase())).open(p);
                } catch (Exception ex) {
                    p.sendMessage(ChatColor.RED + mobData + " is not a valid Minigame!");
                    p.sendMessage(ChatColor.GOLD + Joiner.on(ChatColor.WHITE + ", " + ChatColor.GOLD).join(MinigameType.values()));
                }
            } else if (interactAction.equalsIgnoreCase("connectToServer")) {
                BeastCubeAPI.connect(p, mobData);
            }
        }
    }
}
