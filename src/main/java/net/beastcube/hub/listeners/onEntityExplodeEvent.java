package net.beastcube.hub.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.plugin.Plugin;

/**
 * @author BeastCube
 */
public class onEntityExplodeEvent implements Listener {

    private final Plugin plugin;

    public onEntityExplodeEvent(Plugin plugin) {
        this.plugin = plugin;
    }

    /**
     * Blocks can not be broken by tnt or other explosions
     */
    @EventHandler
    public void onEntityExplode(EntityExplodeEvent e) {
        e.blockList().clear();
    }
}
