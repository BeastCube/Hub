package net.beastcube.hub.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.plugin.Plugin;

/**
 * @author BeastCube
 */
public class onEntityDamageEvent implements Listener {

    private final Plugin plugin;

    public onEntityDamageEvent(Plugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent e) {
        if (e.getCause() == DamageCause.ENTITY_EXPLOSION) {
            //Don't cancel tnt explosion because then it wouldn't throw you in the air
            e.setDamage(0D);
        } else if (e.getCause() != DamageCause.ENTITY_ATTACK) {
            e.setCancelled(true);
        }
    }
}
