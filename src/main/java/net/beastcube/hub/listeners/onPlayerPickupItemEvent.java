package net.beastcube.hub.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.plugin.Plugin;

/**
 * @author BeastCube
 */
public class onPlayerPickupItemEvent implements Listener {

    private final Plugin plugin;

    public onPlayerPickupItemEvent(Plugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerPickupItem(PlayerPickupItemEvent e) {
        e.setCancelled(true);
    }
}
