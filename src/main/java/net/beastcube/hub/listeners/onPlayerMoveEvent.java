package net.beastcube.hub.listeners;

import net.beastcube.hub.Hub;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

/**
 * @author BeastCube
 */
public class onPlayerMoveEvent implements Listener {

    private final Plugin plugin;

    public onPlayerMoveEvent(Plugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        Player p = e.getPlayer();

        if (p.getLocation().getBlock().getType() == Material.STONE_PLATE) {
            if (p.getLocation().subtract(0, 1, 0).getBlock().getType() == Material.REDSTONE_BLOCK) {
                Location loc = e.getPlayer().getLocation().clone();
                loc.setPitch(0F);
                Vector v = loc.getDirection().normalize().multiply(3).setY(1);
                p.setVelocity(v);
                p.playSound(p.getLocation(), Sound.ENDERDRAGON_WINGS, 3, 2);
            }
        }
        if (p.getLocation().getY() < 0 && Hub.getInstance().getHubSettings().getSpawn() != null) {
            e.getPlayer().teleport(Hub.getInstance().getHubSettings().getSpawn());
        }
    }
}
