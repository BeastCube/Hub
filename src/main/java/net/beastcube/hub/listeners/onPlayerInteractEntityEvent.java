package net.beastcube.hub.listeners;

import net.beastcube.hub.hubgadgets.stacker.StackerManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.plugin.Plugin;

/**
 * @author BeastCube
 */
public class onPlayerInteractEntityEvent implements Listener {

    private final Plugin plugin;

    public onPlayerInteractEntityEvent(Plugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent e) {
        Player p = e.getPlayer();
        if (e.getRightClicked() instanceof Player) {
            Player clicked_p = (Player) e.getRightClicked();
            if (p.getItemInHand().getType() == Material.AIR) {
                StackerManager.stack(p, clicked_p);
            }
            e.setCancelled(true);
        }
    }
}
