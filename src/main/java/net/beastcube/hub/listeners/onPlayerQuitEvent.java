package net.beastcube.hub.listeners;

import net.beastcube.hub.Hub;
import net.beastcube.hub.hubgadgets.playerhider.PlayerHiderManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

/**
 * @author BeastCube
 */
public class onPlayerQuitEvent implements Listener {

    private final Plugin plugin;

    public onPlayerQuitEvent(Plugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        if (PlayerHiderManager.hiddenPlayers.containsKey(e.getPlayer().getUniqueId()))
            PlayerHiderManager.hiddenPlayers.remove(e.getPlayer().getUniqueId());
        e.setQuitMessage(null);
        Hub.getInstance().removeStats(e.getPlayer());
    }

}
