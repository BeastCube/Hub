package net.beastcube.hub.listeners;

import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.bukkit.interactitem.InteractItemManager;
import net.beastcube.api.bukkit.util.ItemBuilder;
import net.beastcube.api.commons.packets.UpdateScoreboardRequestPacket;
import net.beastcube.hub.Hub;
import net.beastcube.hub.hubgadgets.playerhider.PlayerHiderManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

/**
 * @author BeastCube
 */
public class onPlayerJoinEvent implements Listener {

    private final Plugin plugin;

    public onPlayerJoinEvent(Plugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent e) {
        Player p = e.getPlayer();
        e.setJoinMessage(null);
        p.setHealth(20D);
        p.setFoodLevel(20);

        if (Hub.getInstance().getHubSettings().getSpawn() != null) {
            p.teleport(Hub.getInstance().getHubSettings().getSpawn());
        }

        p.getInventory().clear();

        //TODO Not implemented
        ItemStack lobbyselector = new ItemBuilder(Material.WATCH).name(ChatColor.YELLOW + "Lobby wählen").lore(ChatColor.GRAY + "Wechsle die Lobby").removeAllFlags().build();

        //NOTE: Slot 6 is being used for the gadget!
        p.getInventory().setItem(1, lobbyselector);
        p.getInventory().setItem(4, Hub.LOBBYTELEPORTER.createItemStack());
        p.getInventory().setItem(7, InteractItemManager.PERSONALCHEST.createItemStack());
        p.getInventory().setItem(8, Hub.playerHiderManager.playerHider.createItemStack());
        p.getInventory().setItem(0, Hub.FRIENDSMENU.createItemStack());
        PlayerHiderManager.updateVisibility(p);
        Bukkit.getScheduler().runTask(plugin, () -> BeastCubeAPI.getGateway().sendPacket(p, new UpdateScoreboardRequestPacket()));
        Hub.getInstance().displayStats(p);
    }

}
