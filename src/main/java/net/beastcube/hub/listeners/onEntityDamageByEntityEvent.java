package net.beastcube.hub.listeners;

import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.commons.packets.friends.ShowPlayerRequestPacket;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.plugin.Plugin;

/**
 * @author BeastCube
 */
public class onEntityDamageByEntityEvent implements Listener {

    private final Plugin plugin;

    public onEntityDamageByEntityEvent(Plugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) { //TODO Not working if player is in gamemode
        if (e.getDamager() instanceof Player && e.getEntity() instanceof Player) {
            Player p = (Player) e.getDamager();
            if (p.getItemInHand().getType() == Material.SKULL_ITEM) {
                BeastCubeAPI.getGateway().sendPacket(p, new ShowPlayerRequestPacket(e.getEntity().getUniqueId()));
            }
        }
        e.setDamage(0);
        //e.setCancelled(true);
    }
}
