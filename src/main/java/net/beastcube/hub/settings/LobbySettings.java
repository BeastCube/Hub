package net.beastcube.hub.settings;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.beastcube.api.bukkit.util.serialization.Serialize;
import net.beastcube.api.commons.minigames.MinigameType;
import net.beastcube.hub.hubgadgets.teleporter.TeleportLocation;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author BeastCube
 */
@NoArgsConstructor
public class LobbySettings {

    @Getter
    @Setter
    @Serialize
    private Location spawn = null;
    @Getter
    @Setter
    @Serialize
    private List<TeleportLocation> warps = new ArrayList<>();
    @Getter
    @Setter
    @Serialize
    private Map<MinigameType, Location> stats = new HashMap<>();

    //--------Getters and Setters--------\\

    public boolean addWarp(TeleportLocation loc) {
        return warps.add(loc);
    }

    public boolean removeWarp(TeleportLocation loc) {
        return warps.remove(loc);
    }

    public void removeWarp(int i) {
        warps.remove(i);
    }

    public void clearWarp() {
        warps.clear();
    }

    public void addStatsHologram(MinigameType minigameType, Location location) {
        stats.put(minigameType, location);
    }

    public void removeStatsHologram(MinigameType minigameType) {
        stats.remove(minigameType);
    }

    public void removeStatsHologram(int i) {
        stats.remove(new ArrayList<>(stats.keySet()).get(i));
    }

    public void clearStatsHologram() {
        stats.clear();
    }

}
