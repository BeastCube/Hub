package net.beastcube.hub.settings;

import com.google.gson.*;
import net.beastcube.hub.hubgadgets.teleporter.TeleportLocation;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Type;

/**
 * @author BeastCube
 */
public class TeleportLocationTypeAdapter implements JsonDeserializer<TeleportLocation>, JsonSerializer<TeleportLocation> {

    @Override
    public TeleportLocation deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject itemObject = jsonElement.getAsJsonObject();
        Location loc = jsonDeserializationContext.deserialize(itemObject.get("location"), Location.class);
        ItemStack itemstack = jsonDeserializationContext.deserialize(itemObject.get("item"), ItemStack.class);
        return new TeleportLocation(itemstack, loc);
    }

    @Override
    public JsonElement serialize(TeleportLocation teleportLocation, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject root = new JsonObject();
        if (teleportLocation != null) {
            root.add("location", jsonSerializationContext.serialize(teleportLocation.getLocation()));
            root.add("item", jsonSerializationContext.serialize(teleportLocation.getItemStack()));
        }
        return root;
    }
}
