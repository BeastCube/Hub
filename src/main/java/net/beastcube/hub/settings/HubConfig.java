package net.beastcube.hub.settings;

import net.beastcube.api.bukkit.config.annotation.AnnotationConfiguration;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

/**
 * @author BeastCube
 */
public class HubConfig extends AnnotationConfiguration {

    public World currentHub;

    private Plugin plugin = null;
    private static FileConfiguration config = null;

    public HubConfig(Plugin plugin) {
        this.plugin = plugin;
        config = plugin.getConfig();
        this.load();
        this.save();
    }

    public void load() {
        load(config);
    }

    public void save() {
        save(config);
        plugin.saveConfig();
    }

    @Override
    public void load(ConfigurationSection section) {
        super.load(section);
        if (section.contains("currentHub")) {
            currentHub = Bukkit.getWorld(section.getString("currentHub"));
        }
    }

    @Override
    public void save(ConfigurationSection section) {
        super.save(section);
        if (currentHub != null) {
            section.set("currentHub", currentHub.getName());
        }
    }

}
